<?
header('Content-Type: text/html;charset=utf-8');
?>
<?php
    $c = OCILogon("scott", "tiger", "orcl"); 
    if ( ! $c ) {
        echo "невозможно подключится к базе: " . var_dump( ociError() );
        die();
    }
// производим выборку из базы данных 

$s = OCIParse($c, "SELECT round(sin(3.14),5) FROM dual");
OCIExecute($s, OCI_DEFAULT);
while (OCIFetch($s))
{
	echo "<br>"."sin(3.14) = ".OCIResult($s,1)."<br>";
}
OCICommit($c);
OCILogoff($c);