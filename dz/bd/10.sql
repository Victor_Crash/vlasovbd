spool create_trigger_update.log;

prompt "�������� ��������� �� ���������� ������";



create or replace trigger t_dep_upd
	before update on departament
		for each row
			begin
				if(:new.id_dep != :old.id_dep ) then
					:new.id_dep := :old.id_dep ;
				end if;	
			end;
/


create or replace trigger t_equip_upd
	before update on equipment
		for each row
			begin 
				if(:new.id_equipment != :old.id_equipment ) then
					:new.id_equipment := :old.id_equipment ;
				end if;	
			end;
/


create or replace trigger t_listop_upd
	before update on list_of_operation
		for each row
			begin 
				if(:new.id_listop != :old.id_listop ) then
					:new.id_listop := :old.id_listop ;
				end if;	
			end;
/


create or replace trigger t_match_upd
	before update on match_equip_op
		for each row
			begin 
				if(:new.id_match != :old.id_match ) then
					:new.id_match := :old.id_match ;
				end if;	
			end;
/


create or replace trigger t_operation_upd
	before update on operations
		for each row
			begin 
				if(:new.id_operation != :old.id_operation ) then
					:new.id_operation := :old.id_operation ;
				end if;	
			end;
/


create or replace trigger t_product_upd
	before update on product
		for each row
			begin 
				if(:new.id_product != :old.id_product ) then
					:new.id_product := :old.id_product ;
				end if;	
			end;
/


create or replace trigger t_prodtype_upd
	before update on products_type
		for each row
			begin 
				if(:new.id_type != :old.id_type ) then
					:new.id_type := :old.id_type ;
				end if;	
			end;
/


create or replace trigger t_resource_upd
	before update on resources
		for each row
			begin 
				if(:new.id_res != :old.id_res ) then
					:new.id_res := :old.id_res ;
				end if;	
			end;
/




create or replace trigger t_resus_upd
	before update on res_usage
		for each row
			begin 
				if(:new.id_resus != :old.id_resus ) then
					:new.id_resus := :old.id_resus ;
				end if;	
			end;
/


create or replace trigger t_staff_upd
	before update on staff
		for each row
			begin 
				if(:new.id_staff != :old.id_staff ) then
					:new.id_staff := :old.id_staff ;
				end if;	
			end;
/


create or replace trigger t_tp_upd
	before update on tp
		for each row
			begin 
				if(:new.id_tp != :old.id_tp ) then
					:new.id_tp := :old.id_tp ;
				end if;	
			end;
/


create or replace trigger t_tp_res_upd
	before update on tp_res_us
		for each row
			begin 
				if(:new.id_tp_res != :old.id_tp_res ) then
					:new.id_tp_res := :old.id_tp_res ;
				end if;	
			end;
/


spool off;