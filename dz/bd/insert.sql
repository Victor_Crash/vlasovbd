
insert into departament(id_dep, name, phone, notes) values(1, 'Podgotovitelnyi', '123-45-67(dob. 101)', 'Otdel zanimaetsya vhodnym kontrolem i podgotovkoi komplektuyuschih');
insert into departament(id_dep, name, phone, notes) values(2, 'Sborochnyi', '123-45-67(dob. 102)', 'Otdel osuschestvlyaet ustnovku ERE, rachnuyu i avtomaticheskuyu paiki');
insert into departament(id_dep, name, phone, notes) values(3, 'Vyhodnoi', '123-45-67(dob. 103)', 'Otdel ochischaet spayannye PP, pokryvaet lakom i vstavlyaet v korpusa');
insert into departament(id_dep, name, phone, notes) values(4, 'Testirovachnyi', '123-45-67(dob. 104)', 'Otdel provodit vyhodnoi funkcionalnyi i elektricheskii kontroli');



insert into staff(fio, pasport, id_dep) values('Antohin Aleksandr', '4345 635434', 1);
insert into staff(fio, pasport, id_dep) values('Antohina Darya', '1234 641507', 1);
insert into staff(fio, pasport, id_dep) values('Balahnova Ekaterina', '8445 384890', 2);
insert into staff(fio, pasport, id_dep) values('Bandin Artem', '2331 479098', 2);
insert into staff(fio, pasport, id_dep) values('Baseckii Oleg', '4778 538428', 2);
insert into staff(fio, pasport, id_dep) values('Belous Evgenii', '2533 564234', 3);
insert into staff(fio, pasport, id_dep) values('Gavrilov Anton', '2778 300955', 3);
insert into staff(fio, pasport, id_dep) values('Juikov Vladimir', '2655 566356', 4);
insert into staff(fio, pasport, id_dep) values('Korovikov Aleksei', '4355 540950', 4);


insert into list_of_operation(name, id_dep) values('Vhodnoi kontrol', 1);
insert into list_of_operation(name, id_dep) values('Formovka', 1);
insert into list_of_operation(name, id_dep) values('Komplektaciya ERE', 1);
insert into list_of_operation(name, id_dep) values('Ustanovka ERE', 2);
insert into list_of_operation(name, id_dep) values('Paika volnoi', 2);
insert into list_of_operation(name, id_dep) values('Paika payalnikom', 2);
insert into list_of_operation(name, id_dep) values('Otmyvka', 3);
insert into list_of_operation(name, id_dep) values('Pokrytie lakom', 3);
insert into list_of_operation(name, id_dep) values('Ustanovka v korpus', 3);
insert into list_of_operation(name, id_dep) values('Elektricheskii kontrol', 4);
insert into list_of_operation(name, id_dep) values('Funkcionalnyi kontrol', 4);
insert into list_of_operation(name, id_dep) values('Upakovka', 3);



insert into equipment(name, inventar_nomer) values('Stol', 112342);
insert into equipment(name, inventar_nomer) values('Montajnyi stol', 435709);
insert into equipment(name, inventar_nomer) values('Formovschik', 398084);
insert into equipment(name, inventar_nomer) values('Lupa', 400987);
insert into equipment(name, inventar_nomer) values('Pincet', 459800);
insert into equipment(name, inventar_nomer) values('Payalnik', 450984);
insert into equipment(name, inventar_nomer) values('Ust. paiki volnoi', 124349);
insert into equipment(name, inventar_nomer) values('Vanna', 390084);
insert into equipment(name, inventar_nomer) values('Ust. lakirovaniya', 454355);
insert into equipment(name, inventar_nomer) values('Otvertka', 450094);
insert into equipment(name, inventar_nomer) values('Ust.  elektr. kontrolya', 100934);
insert into equipment(name, inventar_nomer) values('Ust.  funkc. kontrolya', 490372);
insert into equipment(name, inventar_nomer) values('Upakovschik v plenku', 450094);



insert into match_equip_op(id_listop, id_equipment) values(1, 1);
insert into match_equip_op(id_listop, id_equipment) values(1, 4);
insert into match_equip_op(id_listop, id_equipment) values(2, 1);
insert into match_equip_op(id_listop, id_equipment) values(2, 3);
insert into match_equip_op(id_listop, id_equipment) values(2, 5);
insert into match_equip_op(id_listop, id_equipment) values(3, 1);
insert into match_equip_op(id_listop, id_equipment) values(3, 5);
insert into match_equip_op(id_listop, id_equipment) values(4, 2);
insert into match_equip_op(id_listop, id_equipment) values(4, 5);
insert into match_equip_op(id_listop, id_equipment) values(4, 4);
insert into match_equip_op(id_listop, id_equipment) values(5, 7);
insert into match_equip_op(id_listop, id_equipment) values(6, 2);
insert into match_equip_op(id_listop, id_equipment) values(6, 4);
insert into match_equip_op(id_listop, id_equipment) values(6, 5);
insert into match_equip_op(id_listop, id_equipment) values(6, 6);
insert into match_equip_op(id_listop, id_equipment) values(7, 8);
insert into match_equip_op(id_listop, id_equipment) values(8, 9);
insert into match_equip_op(id_listop, id_equipment) values(9, 2);
insert into match_equip_op(id_listop, id_equipment) values(9, 10);
insert into match_equip_op(id_listop, id_equipment) values(10, 11);
insert into match_equip_op(id_listop, id_equipment) values(11, 12);
insert into match_equip_op(id_listop, id_equipment) values(12, 10);
insert into match_equip_op(id_listop, id_equipment) values(12, 13);



insert into products_type(type_name) values('Akusticheskoe rele');
insert into products_type(type_name) values('Metalloiskatel');



insert into tp(id_type, nom, id_listop) values(1, 1, 1);
insert into tp(id_type, nom, id_listop) values(1, 2, 2);
insert into tp(id_type, nom, id_listop) values(1, 3, 3);
insert into tp(id_type, nom, id_listop) values(1, 4, 4);
insert into tp(id_type, nom, id_listop) values(1, 5, 6);
insert into tp(id_type, nom, id_listop) values(1, 6, 7);
insert into tp(id_type, nom, id_listop) values(1, 7, 10);
insert into tp(id_type, nom, id_listop) values(1, 8, 11);
insert into tp(id_type, nom, id_listop) values(2, 1, 1);
insert into tp(id_type, nom, id_listop) values(2, 2, 2);
insert into tp(id_type, nom, id_listop) values(2, 3, 3);
insert into tp(id_type, nom, id_listop) values(2, 4, 4);
insert into tp(id_type, nom, id_listop) values(2, 5, 5);
insert into tp(id_type, nom, id_listop) values(2, 6, 7);
insert into tp(id_type, nom, id_listop) values(2, 7, 8);
insert into tp(id_type, nom, id_listop) values(2, 8, 9);
insert into tp(id_type, nom, id_listop) values(2, 9, 11);
insert into tp(id_type, nom, id_listop) values(2, 10, 12);




insert into resources(name) values('Rezistor 1.3 kOm');
insert into resources(name) values('Rezistor 5.6 kOm');
insert into resources(name) values('Rezistor 3.2 kOm');
insert into resources(name) values('Rezistor 1.7 kOm');
insert into resources(name) values('Rezistor 15 Om; 2 Vt');
insert into resources(name) values('Kondensator 110 nF');
insert into resources(name) values('Kondensator 340 pF');
insert into resources(name) values('Kondensator 10 mkF');
insert into resources(name) values('Kondensator 300 mkF; 30 V');
insert into resources(name) values('Kondensator 150 mkF; 12 V');
insert into resources(name) values('Kondensator 600 nF');
insert into resources(name) values('IMS TDA2005');
insert into resources(name) values('IMS K567LA7');
insert into resources(name) values('IMS K561LE5');
insert into resources(name) values('Tr-r KD522B');
insert into resources(name) values('Tr-r KT211A');
insert into resources(name) values('Dinamik 5 Vt');
insert into resources(name) values('Pripoi POS 61');
insert into resources(name) values('Spirt 97%');
insert into resources(name) values('Aceton');
insert into resources(name) values('Lak');
insert into resources(name) values('Provod');
insert into resources(name) values('Plenka');
insert into resources(name) values('Schup');
insert into resources(name) values('Korpus 24B');



insert into res_usage(id_res, rcount) values(1, '5 sht.');
insert into res_usage(id_res, rcount) values(2, '2 sht.');
insert into res_usage(id_res, rcount) values(4, '1 sht.');
insert into res_usage(id_res, rcount) values(5, '2 sht.');
insert into res_usage(id_res, rcount) values(6, '3 sht.');
insert into res_usage(id_res, rcount) values(8, '2 sht.');
insert into res_usage(id_res, rcount) values(9, '4 sht.');
insert into res_usage(id_res, rcount) values(12, '1 sht.');
insert into res_usage(id_res, rcount) values(18, '40 gr.');
insert into res_usage(id_res, rcount) values(19, '100 gr.');
insert into res_usage(id_res, rcount) values(20, '30 gr.');
insert into res_usage(id_res, rcount) values(22, '150 sm.');
insert into res_usage(id_res, rcount) values(1, '2 sht.');
insert into res_usage(id_res, rcount) values(3, '3 sht.');
insert into res_usage(id_res, rcount) values(4, '2 sht.');
insert into res_usage(id_res, rcount) values(7, '1 sht.');
insert into res_usage(id_res, rcount) values(8 , '1 sht.');
insert into res_usage(id_res, rcount) values(10, '2 sht.');
insert into res_usage(id_res, rcount) values(11, '5 sht.');
insert into res_usage(id_res, rcount) values(13, '2 sht.');
insert into res_usage(id_res, rcount) values(14, '2 sht.');
insert into res_usage(id_res, rcount) values(15, '4 sht.');
insert into res_usage(id_res, rcount) values(16, '2 sht.');
insert into res_usage(id_res, rcount) values(17, '1 sht.');
insert into res_usage(id_res, rcount) values(18, '200 gr.');
insert into res_usage(id_res, rcount) values(19, '20 gr.');
insert into res_usage(id_res, rcount) values(20, '50 gr.');
insert into res_usage(id_res, rcount) values(21, '45 gr.');
insert into res_usage(id_res, rcount) values(23, '70 sm');
insert into res_usage(id_res, rcount) values(24, '1 sht.');
insert into res_usage(id_res, rcount) values(25, '1 sht.');



insert into tp_res_us(id_tp, id_resus) values(1, 1);
insert into tp_res_us(id_tp, id_resus) values(1, 2);
insert into tp_res_us(id_tp, id_resus) values(1, 3);
insert into tp_res_us(id_tp, id_resus) values(1, 4);
insert into tp_res_us(id_tp, id_resus) values(1, 5);
insert into tp_res_us(id_tp, id_resus) values(1, 6);
insert into tp_res_us(id_tp, id_resus) values(1, 7);
insert into tp_res_us(id_tp, id_resus) values(1, 8);

insert into tp_res_us(id_tp, id_resus) values(2, 1);
insert into tp_res_us(id_tp, id_resus) values(2, 2);
insert into tp_res_us(id_tp, id_resus) values(2, 3);
insert into tp_res_us(id_tp, id_resus) values(2, 4);
insert into tp_res_us(id_tp, id_resus) values(2, 5);
insert into tp_res_us(id_tp, id_resus) values(2, 6);
insert into tp_res_us(id_tp, id_resus) values(2, 7);

insert into tp_res_us(id_tp, id_resus) values(3, 1);
insert into tp_res_us(id_tp, id_resus) values(3, 2);
insert into tp_res_us(id_tp, id_resus) values(3, 3);
insert into tp_res_us(id_tp, id_resus) values(3, 4);
insert into tp_res_us(id_tp, id_resus) values(3, 5);
insert into tp_res_us(id_tp, id_resus) values(3, 6);
insert into tp_res_us(id_tp, id_resus) values(3, 7);
insert into tp_res_us(id_tp, id_resus) values(3, 8);
insert into tp_res_us(id_tp, id_resus) values(3, 12);

insert into tp_res_us(id_tp, id_resus) values(4, 1);
insert into tp_res_us(id_tp, id_resus) values(4, 2);
insert into tp_res_us(id_tp, id_resus) values(4, 3);
insert into tp_res_us(id_tp, id_resus) values(4, 4);
insert into tp_res_us(id_tp, id_resus) values(4, 5);
insert into tp_res_us(id_tp, id_resus) values(4, 6);
insert into tp_res_us(id_tp, id_resus) values(4, 7);
insert into tp_res_us(id_tp, id_resus) values(4, 8);
insert into tp_res_us(id_tp, id_resus) values(4, 12);

insert into tp_res_us(id_tp, id_resus) values(5, 9);
insert into tp_res_us(id_tp, id_resus) values(5, 10);

insert into tp_res_us(id_tp, id_resus) values(6, 11);

insert into tp_res_us(id_tp, id_resus) values(9, 13);
insert into tp_res_us(id_tp, id_resus) values(9, 2);
insert into tp_res_us(id_tp, id_resus) values(9, 15);
insert into tp_res_us(id_tp, id_resus) values(9, 16);
insert into tp_res_us(id_tp, id_resus) values(9, 17);
insert into tp_res_us(id_tp, id_resus) values(9, 18);
insert into tp_res_us(id_tp, id_resus) values(9, 19);
insert into tp_res_us(id_tp, id_resus) values(9, 20);
insert into tp_res_us(id_tp, id_resus) values(9, 21);
insert into tp_res_us(id_tp, id_resus) values(9, 22);
insert into tp_res_us(id_tp, id_resus) values(9, 23);
insert into tp_res_us(id_tp, id_resus) values(9, 24);


insert into tp_res_us(id_tp, id_resus) values(10, 13);
insert into tp_res_us(id_tp, id_resus) values(10, 2);
insert into tp_res_us(id_tp, id_resus) values(10, 15);
insert into tp_res_us(id_tp, id_resus) values(10, 16);
insert into tp_res_us(id_tp, id_resus) values(10, 17);
insert into tp_res_us(id_tp, id_resus) values(10, 18);
insert into tp_res_us(id_tp, id_resus) values(10, 19);


insert into tp_res_us(id_tp, id_resus) values(11, 13);
insert into tp_res_us(id_tp, id_resus) values(11, 2);
insert into tp_res_us(id_tp, id_resus) values(11, 15);
insert into tp_res_us(id_tp, id_resus) values(11, 16);
insert into tp_res_us(id_tp, id_resus) values(11, 17);
insert into tp_res_us(id_tp, id_resus) values(11, 18);
insert into tp_res_us(id_tp, id_resus) values(11, 19);
insert into tp_res_us(id_tp, id_resus) values(11, 20);
insert into tp_res_us(id_tp, id_resus) values(11, 21);
insert into tp_res_us(id_tp, id_resus) values(11, 22);
insert into tp_res_us(id_tp, id_resus) values(11, 23);
insert into tp_res_us(id_tp, id_resus) values(11, 24);


insert into tp_res_us(id_tp, id_resus) values(12, 13);
insert into tp_res_us(id_tp, id_resus) values(12, 2);
insert into tp_res_us(id_tp, id_resus) values(12, 15);
insert into tp_res_us(id_tp, id_resus) values(12, 16);
insert into tp_res_us(id_tp, id_resus) values(12, 17);
insert into tp_res_us(id_tp, id_resus) values(12, 18);
insert into tp_res_us(id_tp, id_resus) values(12, 19);
insert into tp_res_us(id_tp, id_resus) values(12, 20);
insert into tp_res_us(id_tp, id_resus) values(12, 21);
insert into tp_res_us(id_tp, id_resus) values(12, 22);
insert into tp_res_us(id_tp, id_resus) values(12, 23);
insert into tp_res_us(id_tp, id_resus) values(12, 24);


insert into tp_res_us(id_tp, id_resus) values(13, 25);
insert into tp_res_us(id_tp, id_resus) values(13, 26);

insert into tp_res_us(id_tp, id_resus) values(14, 27);

insert into tp_res_us(id_tp, id_resus) values(15, 28);

insert into tp_res_us(id_tp, id_resus) values(16, 30);
insert into tp_res_us(id_tp, id_resus) values(16, 31);

insert into tp_res_us(id_tp, id_resus) values(18, 29);

