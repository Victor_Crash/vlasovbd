spool create_fk.log;

prompt "�������� ����������� �� ��������� ����";


ALTER TABLE List_of_operation
	DROP CONSTRAINT R_15;
ALTER TABLE List_of_operation
	ADD (CONSTRAINT  R_15 FOREIGN KEY (id_dep) REFERENCES Departament(id_dep));

ALTER TABLE Match_equip_op
	DROP CONSTRAINT R_8;
ALTER TABLE Match_equip_op
	ADD (CONSTRAINT  R_8 FOREIGN KEY (id_listop) REFERENCES List_of_operation(id_listop));

ALTER TABLE Match_equip_op
	DROP CONSTRAINT R_9;
ALTER TABLE Match_equip_op
	ADD (CONSTRAINT  R_9 FOREIGN KEY (id_equipment) REFERENCES Equipment(id_equipment));

ALTER TABLE Operations
	DROP CONSTRAINT R_5;
ALTER TABLE Operations
	ADD (CONSTRAINT  R_5 FOREIGN KEY (id_product) REFERENCES Product(id_product));

ALTER TABLE Operations
	DROP CONSTRAINT R_6;
ALTER TABLE Operations
	ADD (CONSTRAINT  R_6 FOREIGN KEY (id_listop) REFERENCES List_of_operation(id_listop));

ALTER TABLE Operations
	DROP CONSTRAINT R_2;
ALTER TABLE Operations
	ADD (CONSTRAINT  R_23 FOREIGN KEY (id_staff) REFERENCES Staff(id_staff));


ALTER TABLE Product
	DROP CONSTRAINT R_25;
ALTER TABLE Product
	ADD (CONSTRAINT  R_25 FOREIGN KEY (id_type) REFERENCES Products_type(id_type));

ALTER TABLE Res_usage
	DROP CONSTRAINT R_32;
ALTER TABLE Res_usage
	ADD (CONSTRAINT  R_32 FOREIGN KEY (id_res) REFERENCES Resources(id_res));

ALTER TABLE Staff
	DROP CONSTRAINT R_14;
ALTER TABLE Staff
	ADD (CONSTRAINT  R_14 FOREIGN KEY (id_dep) REFERENCES Departament(id_dep));

ALTER TABLE TP
	DROP CONSTRAINT R_21;
ALTER TABLE TP
	ADD (CONSTRAINT  R_21 FOREIGN KEY (id_listop) REFERENCES List_of_operation(id_listop));

ALTER TABLE TP
	DROP CONSTRAINT R_22;
ALTER TABLE TP
	ADD (CONSTRAINT  R_22 FOREIGN KEY (id_type) REFERENCES Products_type(id_type));

ALTER TABLE TP_res_us
	DROP CONSTRAINT R_37;	
ALTER TABLE TP_res_us
	ADD (CONSTRAINT  R_37 FOREIGN KEY (id_tp) REFERENCES TP(id_tp));
	
ALTER TABLE TP_res_us
	DROP CONSTRAINT R_38;		
ALTER TABLE TP_res_us
	ADD (CONSTRAINT  R_38 FOREIGN KEY (id_resus) REFERENCES Res_usage(id_resus));
	
	
	
spool off;
