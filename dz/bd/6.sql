spool create_pk.log;

prompt "�������� ��������� ������";


ALTER TABLE Departament
	DROP PRIMARY KEY;
ALTER TABLE Departament
	ADD CONSTRAINT  XPKDepartament PRIMARY KEY (id_dep);
	
ALTER TABLE Equipment
	DROP PRIMARY KEY;	
ALTER TABLE Equipment
	ADD CONSTRAINT  XPKEquipment PRIMARY KEY (id_equipment);
	
ALTER TABLE List_of_operation
	DROP PRIMARY KEY;	
ALTER TABLE List_of_operation
	ADD CONSTRAINT  XPKList_of_operation PRIMARY KEY (id_listop);
	
ALTER TABLE Match_equip_op
	DROP PRIMARY KEY;	
ALTER TABLE Match_equip_op
	ADD CONSTRAINT  XPKMatch_equip_op PRIMARY KEY (id_match);
	
ALTER TABLE Operations
	DROP PRIMARY KEY;	
ALTER TABLE Operations
	ADD CONSTRAINT  XPKOperation PRIMARY KEY (id_operation);
	
ALTER TABLE Product
	DROP PRIMARY KEY;	
ALTER TABLE Product
	ADD CONSTRAINT  XPKProduct PRIMARY KEY (id_product);
	
ALTER TABLE Products_type
	DROP PRIMARY KEY;	
ALTER TABLE Products_type
	ADD CONSTRAINT  XPKProducts_type PRIMARY KEY (id_type);
	
	
ALTER TABLE Res_usage
	DROP PRIMARY KEY;	
ALTER TABLE Res_usage
	ADD CONSTRAINT  XPKCons_usage PRIMARY KEY (id_resus);
	
ALTER TABLE Resources
	DROP PRIMARY KEY;	
ALTER TABLE Resources
	ADD CONSTRAINT  XPKConsumables PRIMARY KEY (id_res);
	
ALTER TABLE Staff
	DROP PRIMARY KEY;	
ALTER TABLE Staff
	ADD CONSTRAINT  XPKStaff PRIMARY KEY (id_staff);
	
ALTER TABLE TP
	DROP PRIMARY KEY;	
ALTER TABLE TP
	ADD CONSTRAINT  XPKTP PRIMARY KEY (id_tp);
	
ALTER TABLE TP_res_us
	DROP PRIMARY KEY;
ALTER TABLE TP_res_us
	ADD CONSTRAINT  XPKTP_RES_US PRIMARY KEY (id_tp_res);
	
	
spool off;