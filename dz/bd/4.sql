spool create_table.log;

prompt "�������� ������";

DROP TABLE Departament CASCADE CONSTRAINTS;
CREATE TABLE Departament
(
	id_dep  INTEGER  NOT NULL ,
	name  VARCHAR2(40)  NULL ,
	phone  VARCHAR2(40)  NULL ,
	notes  VARCHAR2(200)  NULL ,
	deleted INTEGER DEFAULT 1
);
DROP TABLE Equipment CASCADE CONSTRAINTS;
CREATE TABLE Equipment
(
	id_equipment  INTEGER  NOT NULL ,
	inventar_nomer  INTEGER  NULL ,
	name  VARCHAR2(40)  NULL ,
	notes  VARCHAR2(200)  NULL, 
	deleted INTEGER DEFAULT 1 
);

DROP TABLE List_of_operation CASCADE CONSTRAINTS;
CREATE TABLE List_of_operation
(
	id_listop  INTEGER  NOT NULL ,
	name  VARCHAR2(40)  NULL ,
	notes  VARCHAR2(200)  NULL ,
	id_dep  INTEGER  NULL ,
	deleted INTEGER DEFAULT 1 
);
DROP TABLE Match_equip_op CASCADE CONSTRAINTS;
CREATE TABLE Match_equip_op
(
	id_match  INTEGER  NOT NULL ,
	id_listop  INTEGER  NULL ,
	id_equipment  INTEGER  NULL ,
	notes  VARCHAR2(200)  NULL ,
	deleted INTEGER DEFAULT 1 
);
DROP TABLE Operations CASCADE CONSTRAINTS;
CREATE TABLE Operations
(
	id_operation  INTEGER  NOT NULL ,
	id_product  INTEGER  NOT NULL ,
	id_listop  INTEGER  NULL ,
	op_status  INTEGER  NULL ,
	op_start_date  DATE  NULL ,
	op_end_date  DATE  NULL ,
	notes  VARCHAR2(200)  NULL ,
	id_staff  INTEGER  NULL ,
	op_status_str  VARCHAR2(40)  NULL ,
	deleted INTEGER DEFAULT 1 
);
DROP TABLE Product CASCADE CONSTRAINTS;
CREATE TABLE Product
(
	id_product  INTEGER  NOT NULL ,
	status  INTEGER  NULL ,
	start_date  DATE  NULL ,
	end_date  DATE  NULL ,
	notes  VARCHAR2(200)  NULL ,
	id_type  INTEGER  NULL ,
	status_str  VARCHAR2(40)  NULL ,
	deleted INTEGER DEFAULT 1 
);
DROP TABLE Products_type CASCADE CONSTRAINTS;
CREATE TABLE Products_type
(
	id_type  INTEGER  NOT NULL ,
	type_name  VARCHAR2(40)  NULL ,
	notes  VARCHAR2(200)  NULL ,
	deleted INTEGER DEFAULT 1 
);

DROP TABLE Res_usage CASCADE CONSTRAINTS;
CREATE TABLE Res_usage
(
	id_resus  INTEGER  NOT NULL ,
	notes  VARCHAR2(200)  NULL ,
	id_res INTEGER  NULL ,
	rcount  VARCHAR2(40)  NULL ,
	deleted INTEGER DEFAULT 1 
);
DROP TABLE Resources CASCADE CONSTRAINTS;
CREATE TABLE Resources
(
	id_res  INTEGER  NOT NULL ,
	name  VARCHAR2(40)  NULL ,
	notes  VARCHAR2(200)  NULL ,
	deleted INTEGER DEFAULT 1 
);
DROP TABLE Staff CASCADE CONSTRAINTS;
CREATE TABLE Staff
(
	id_staff  INTEGER  NOT NULL ,
	fio  VARCHAR2(40)  NULL ,
	pasport  VARCHAR2(40)  NULL ,
	id_dep  INTEGER  NULL ,
	notes  VARCHAR2(200)  NULL ,
	deleted INTEGER DEFAULT 1 
);
DROP TABLE TP CASCADE CONSTRAINTS;
CREATE TABLE TP
(
	id_tp  INTEGER  NOT NULL ,
	id_listop  INTEGER  NULL ,
	id_type  INTEGER  NULL ,
	nom  INTEGER  NULL ,
	notes  VARCHAR2(200)  NULL ,
	deleted INTEGER DEFAULT 1 
);
DROP TABLE TP_res_us CASCADE CONSTRAINTS;
CREATE TABLE TP_res_us
(
	id_tp_res  INTEGER  NOT NULL ,
	id_tp  INTEGER  NULL ,
	id_resus  INTEGER  NULL 
);


spool off;