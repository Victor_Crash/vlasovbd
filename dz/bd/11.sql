create or replace view stat as
select products_type.id_type,products_type.type_name, stat.all_p, stat.made, stat.brak, stat.deleted,
round(((stat.brak/(stat.all_p-stat.deleted))*100),2) as pr_brak
 from (select id_type, 
(select count(pr1.id_product) from product pr1 where pr1.id_type=pr.id_type) as all_p,
(select count(pr2.id_product) from product pr2 where pr2.id_type=pr.id_type and pr2.status=1000) as made,
(select count(pr3.id_product) from product pr3 where pr3.id_type=pr.id_type and pr3.status=-1) as brak,
(select count(pr4.id_product) from product pr4 where pr4.id_type=pr.id_type and pr4.status=-2) as deleted  
 from product pr group by pr.id_type) stat 
 join products_type on products_type.id_type=stat.id_type;
 