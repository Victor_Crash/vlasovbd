spool create_sequence.log;

prompt "создание последовательностей";


DROP SEQUENCE s_id_dep ;
CREATE SEQUENCE s_id_dep ;

DROP SEQUENCE s_id_equipment ;
CREATE SEQUENCE s_id_equipment ;

DROP SEQUENCE s_id_listop ;
CREATE SEQUENCE s_id_listop ;

DROP SEQUENCE s_id_match ;
CREATE SEQUENCE s_id_match ;

DROP SEQUENCE s_id_operation ;
CREATE SEQUENCE s_id_operation ;

DROP SEQUENCE s_id_product ;
CREATE SEQUENCE s_id_product ;

DROP SEQUENCE s_id_type ;
CREATE SEQUENCE s_id_type ;


DROP SEQUENCE s_id_resus ;
CREATE SEQUENCE s_id_resus ;

DROP SEQUENCE s_id_res ;
CREATE SEQUENCE s_id_res ;

DROP SEQUENCE s_id_staff ;
CREATE SEQUENCE s_id_staff ;

DROP SEQUENCE s_id_tp ;
CREATE SEQUENCE s_id_tp ;

DROP SEQUENCE s_id_tp_res ;
CREATE SEQUENCE s_id_tp_res ;


spool off;