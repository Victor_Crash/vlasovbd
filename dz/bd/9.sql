spool create_trigger_insert.log;

prompt "�������� ��������� �� ������� ������";



CREATE OR REPLACE TRIGGER t_dep_ins
	BEFORE INSERT ON  departament
		FOR EACH ROW
			BEGIN 
				SELECT s_id_dep.nextval
					INTO :new.id_dep
					FROM dual;
			END;
/


CREATE OR REPLACE TRIGGER t_equip_ins
	BEFORE INSERT ON  equipment
		FOR EACH ROW
			BEGIN 
				SELECT s_id_equipment.nextval
					INTO :new.id_equipment
					FROM dual;
			END;
/


CREATE OR REPLACE TRIGGER t_listop_ins
	BEFORE INSERT ON  list_of_operation
		FOR EACH ROW
			BEGIN 
				SELECT s_id_listop.nextval
					INTO :new.id_listop
					FROM dual;
			END;
/


CREATE OR REPLACE TRIGGER t_match_ins
	BEFORE INSERT ON  match_equip_op
		FOR EACH ROW
			BEGIN 
				SELECT s_id_match.nextval
					INTO :new.id_match
					FROM dual;
			END;
/


CREATE OR REPLACE TRIGGER t_operation_ins
	BEFORE INSERT ON  operations
		FOR EACH ROW
			BEGIN 
				SELECT s_id_operation.nextval
					INTO :new.id_operation
					FROM dual;
			END;
/


CREATE OR REPLACE TRIGGER t_product_ins
	BEFORE INSERT ON  product
		FOR EACH ROW
			BEGIN 
				SELECT s_id_product.nextval
					INTO :new.id_product
					FROM dual;
			END;
/


CREATE OR REPLACE TRIGGER t_prodtype_ins
	BEFORE INSERT ON  products_type
		FOR EACH ROW
			BEGIN 
				SELECT s_id_type.nextval
					INTO :new.id_type
					FROM dual;
			END;
/


CREATE OR REPLACE TRIGGER t_resource_ins
	BEFORE INSERT ON  resources
		FOR EACH ROW
			BEGIN 
				SELECT s_id_res.nextval
					INTO :new.id_res
					FROM dual;
			END;
/




CREATE OR REPLACE TRIGGER t_resus_ins
	BEFORE INSERT ON  res_usage
		FOR EACH ROW
			BEGIN 
				SELECT s_id_resus.nextval
					INTO :new.id_resus
					FROM dual;
			END;
/


CREATE OR REPLACE TRIGGER t_staff_ins
	BEFORE INSERT ON  staff
		FOR EACH ROW
			BEGIN 
				SELECT s_id_staff.nextval
					INTO :new.id_staff
					FROM dual;
			END;
/


CREATE OR REPLACE TRIGGER t_tp_ins
	BEFORE INSERT ON  tp
		FOR EACH ROW
			BEGIN 
				SELECT s_id_tp.nextval
					INTO :new.id_tp
					FROM dual;
			END;
/


CREATE OR REPLACE TRIGGER t_tp_res_ins
	BEFORE INSERT ON  tp_res_us
		FOR EACH ROW
			BEGIN 
				SELECT s_id_tp_res.nextval
					INTO :new.id_tp_res
					FROM dual;
			END;
/


spool off;