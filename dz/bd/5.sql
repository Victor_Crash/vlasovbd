spool create_index.log;

prompt "создание индексов";

DROP INDEX XPKDepartament;
CREATE  UNIQUE INDEX XPKDepartament ON Departament
(
	id_dep  ASC
);
DROP INDEX XPKEquipment;
CREATE  UNIQUE INDEX XPKEquipment ON Equipment
(
	id_equipment  ASC
);
DROP INDEX XPKList_of_operation;
CREATE  UNIQUE INDEX XPKList_of_operation ON List_of_operation
(
	id_listop  ASC
);
DROP INDEX XPKMatch_equip_op;
CREATE  UNIQUE INDEX XPKMatch_equip_op ON Match_equip_op
(
	id_match  ASC
);
DROP INDEX XPKOperation;
CREATE  UNIQUE INDEX XPKOperation ON Operations
(
	id_operation  ASC
);
DROP INDEX XPKProduct;
CREATE  UNIQUE INDEX XPKProduct ON Product
(
	id_product  ASC
);
DROP INDEX XPKProducts_type;
CREATE  UNIQUE INDEX XPKProducts_type ON Products_type
(
	id_type  ASC
);

DROP INDEX XPKCons_usage;
CREATE  UNIQUE INDEX XPKCons_usage ON Res_usage
(
	id_resus  ASC
);
DROP INDEX XPKConsumables;
CREATE  UNIQUE INDEX XPKConsumables ON Resources
(
	id_res  ASC
);
DROP INDEX XPKStaff;
CREATE  UNIQUE INDEX XPKStaff ON Staff
(
	id_staff  ASC
);
DROP INDEX XPKTP;
CREATE  UNIQUE INDEX XPKTP ON TP
(
	id_tp  ASC
);
DROP INDEX XPKTP_RES_US;
CREATE  UNIQUE INDEX XPKTP_RES_US ON TP_res_us
(
	id_tp_res  ASC
);


spool off;