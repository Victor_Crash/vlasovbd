spool create_role.log;

prompt "�������� ���� role_admin";


drop role "role_admin";
create role "role_admin";

grant create any index to "role_admin";
grant create any procedure to "role_admin";
grant create any sequence to "role_admin";
grant create any synonym to "role_admin";
grant create any table to "role_admin";
grant create any trigger to "role_admin";
grant create any view to "role_admin";
grant create procedure to "role_admin";
grant create public synonym to "role_admin";
grant create sequence to "role_admin";
grant create table to "role_admin";
grant create trigger to "role_admin";
grant create view to "role_admin";

grant alter any index to "role_admin";
grant alter any procedure to "role_admin";
grant alter any sequence to "role_admin";
grant alter any table to "role_admin";
grant alter any trigger to "role_admin";

grant drop any procedure to "role_admin";
grant drop any sequence to "role_admin";
grant drop any table to "role_admin";
grant drop any trigger to "role_admin";
grant drop any view to "role_admin";

grant execute any procedure to "role_admin";

grant "CONNECT" to "role_admin";
grant "DBA" to "role_admin";


spool off;


